'******************************************************************************
' BODY EXPORT MACRO
'
' Created by Aron Dahlgren on 2018-10-17 - Tested with Windows 10 using Solidworks 2017 x64 sp3
' Updated by Aron Dahlgren on 2018-10-31 - Tested with Windows 10 using Solidworks 2017 x64 sp3
' Updated by Aron Dahlgren on 2019-07-30 - Tested with Windows 10 using Solidworks 2019 x64 sp3

'
' In a Solidworks multibody part file, make visible all the bodies you would like to export.
' Hide all other files. Give the visible bodies in the feature tree names as they will be used
' as part of the file name. Execute the macro. Select the format in which you would
' like to export the file. Each visible body will be individually saved to the user's
' desktop as an STL or STEP file. The file name will include the part name, body name, and number of
' occurances in the save folder.
'
' This tool also works for Solidworks single body part files. Functionality is similar to that
' of a multbody part file but the user doesn't have the abilty to name the bodies in the feature
' tree. The exported file will just be given the same name as the part.
'
'******************************************************************************

Option Explicit

    Dim swApp As SldWorks.SldWorks
    Dim swModel As SldWorks.ModelDoc2
	Dim swSelMgr As SldWorks.SelectionMgr
	Dim swModelDocExt As SldWorks.ModelDocExtension
	Dim Part As Object
	
	Dim vBodies As Variant
	Dim vBodiesTotal As Variant
	Dim bodyName As String
	
	Dim i As Long
	Dim FileNameCheck As String
    Dim FilePath As String	
    Dim FileName As StrinG	
    Public FileExtension As String 'Needs to be public so user form can access
	
	Dim saveincrement As Integer	
	Dim documentName As String	
	Dim outFile As String	
    Dim boolstatus As Boolean
    Dim userName As String
    Dim opt As Long
    Dim lErrors As Long
    Dim lWarnings As Long




Sub main()

	Set swApp = Application.SldWorks
    Set swModel = swApp.ActiveDoc
    Set swModelDocExt = swModel.Extension
	Set Part = swApp.ActiveDoc

	
	'BUILD THE NAME OF THE SAVED FILE
	'gets user name so file can be saved to desktop
	userName = Environ("USERNAME")
	'gets the name and path of the part
	documentName = Left(Part.GetTitle, Len(Part.GetTitle) - 7)
	FilePath = "C:\Users\" & userName & "\Desktop\"
	FileExtension = "exit" 'This can be STL, STEP, IGES. Exit is the initial value prior to user selection.
	'FileName = FilePath & documentName & EXT 'REMOVE??
	'SAVE OPTIONS
	'1 =    swSaveAsOptions_Silent
    '4 =    swSaveAsOptions_SaveReferenced
    '8 =    swSaveAsOptions_AvoidRebuildOnSave
    '512 =  swSaveAsOptions_CopyAndOpen
	opt = 8
	
	
	'LET USER PICK THE TYPE OF FILE THEY WANT TO EXPORT
	'Find the current size of the Solidworks window and determine where to place the user selection form
	'Form setting startlocation has to be 0-manual in order for this to work
	Load exportSelectionForm
	exportSelectionForm.Top = (((swApp.FrameHeight / 20) * 15) / 2) - (exportSelectionForm.Height / 2) + ((swApp.FrameTop / 20) * 15)
	exportSelectionForm.Left = (((swApp.FrameWidth / 20) * 15) / 2) - (exportSelectionForm.Width / 2) + ((swApp.FrameLeft / 20) * 15)

	'User selection form. User simply sets the file extention for export
	exportSelectionForm.Show

	'If FileExtension isn't changed in the user selection form, exit the sub
	If FileExtension = "exit" Then Exit Sub
	
	
	'GET BODIES
    'Get the not hidden and hidden bodies
	vBodies = Part.GetBodies2(swSolidBody, True)
	vBodiesTotal = Part.GetBodies2(swSolidBody, False)
	'MsgBox "total visible" & UBound(vBodies)
	'MsgBox "total" & UBound(vBodiesTotal)

	'If there are no bodies, display an error message
	If IsEmpty(vBodies) Then
		MsgBox "No bodies are present or visible."
		Exit Sub
	End If
	
	
	'SAVE BODIES
	'If this case is true, this is a single body part
	If (UBound(vBodies) = 0 And UBound(vBodiesTotal) = 0) Then
		'reset the duplicate part number incrementer
		saveincrement = 0
		FileName = documentName + "." + FileExtension
		FileNameCheck = Dir(FilePath + FileName)
		While FileName = FileNameCheck
			saveincrement = saveincrement + 1
			FileName = documentName + "(" & saveincrement & ")." + FileExtension
			FileNameCheck = Dir(FilePath + FileName)
		Wend
		
		'Combine the FilePath and the File name
		outFile = FilePath + FileName
			
		'Save As
		boolstatus = swModelDocExt.SaveAs2(outFile, 0, opt, Nothing, "", False, lErrors, lWarnings)

	Else
		'Go through the list of visible bodies and export each one
		For i = 0 To UBound(vBodies)
			'reset the duplicate part number incrementer
			saveincrement = 0
			
			'Hide all bodies
			boolstatus = Part.Extension.SelectByID2("Solid Bodies", "BDYFOLDER", 0, 0, 0, False, 0, Nothing, 0)
			Part.FeatureManager.HideBodies
			
			'show one of the originally visible bodies
			bodyName = vBodies(i).Name
			Part.ClearSelection2 True
			boolstatus = Part.Extension.SelectByID2(bodyName, "SOLIDBODY", 0, 0, 0, False, 0, Nothing, 0)
			Part.FeatureManager.ShowBodies
			
			'A STEP file needs to be visible and selected for the save method to work. If a face is selected,
            'then an export to an STL might fail.
            If FileExtension = "STEP" Then
                boolstatus = Part.Extension.SelectByID2(bodyName, "SOLIDBODY", 0, 0, 0, False, 0, Nothing, 0)
            End If
			
			
			FileName = documentName + "-" & bodyName & "." + FileExtension
			'Check to see if Filename already exists in directory
			FileNameCheck = Dir(FilePath + FileName)
			'If the file already exists in the directory, add an increment to the file name
			While FileName = FileNameCheck
				saveincrement = saveincrement + 1
				FileName = documentName + "-" & bodyName & " (" & saveincrement & ")." + FileExtension
				FileNameCheck = Dir(FilePath + FileName)
			Wend
			
			'Combine the FilePath and the File name
			outFile = FilePath + FileName
				
			'Save As
			boolstatus = swModelDocExt.SaveAs2(outFile, 0, opt, Nothing, "", False, lErrors, lWarnings)
			
		Next i
	End If
	
	
	'MAKE PREVIOUSLY VISIBLE BODIES VISIBLE AGAIN
	For i = 0 To UBound(vBodies)
		bodyName = vBodies(i).Name
		boolstatus = Part.Extension.SelectByID2(bodyName, "SOLIDBODY", 0, 0, 0, False, 0, Nothing, 0)
		Part.FeatureManager.ShowBodies
	Next i

	
	'SUCCESS MESSAGE
	If UBound(vBodies) = 0 Then
		MsgBox "Saved 1 body as an " & FileExtension & " file to " + FilePath
	Else
		MsgBox "Saved " & UBound(vBodies) + 1 & " bodies as " & FileExtension & " files to " + FilePath
	End If

	'Clears the selection of the part before ending the macro
	Part.ClearSelection2 True

End Sub