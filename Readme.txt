BODY EXPORT MACRO
-----------------

OVERVIEW:
In a Solidworks multibody part file, make visible all the bodies you would like to export. Hide all other files. Give the visible bodies in the feature tree names as they will be used as part of the file name. Execute the macro. Select the format in which you would like to export the file. Each visible body will be individually saved to the user's desktop as an STL or STEP file. The file name will include the part name, body name, and number of occurances in the save folder. This tool also works for Solidworks single body part files. Functionality is similar to that of a multbody part file but the user doesn't have the abilty to name the bodies in the feature tree. The exported file will just be given the same name as the part.


INSTALLATION
Copy the latest version of the Body Export Macro swp file from the \release folder to your local Solidworks macro directory. Usually, this is C:\Program Files\SOLIDWORKS Corp\SOLIDWORKS\Macros but it may be in a different location. You can activate the macro when a part file is open by selecting Tools > Macro > Run. Select the newly installed macro from the directory.

If you want an button for the macro on you Solidworks toolbar, also copy either export_icon.bmp or export_icon2.bmp to the Solidworks macro directory. With a document open in Solidworks, click Tools > Customize. On the Commands tab, select Macro. To the right of the categories, there will be a heading for Buttons. Drag the New Macro Button (hover over it to see its name) to any toolbar in the Solidworks window. A new dialog box, Customize Macro Button, will pop up. Under Action, click the "..." button next to the Macro entry. Select the newly installed macro. Under Appearance, click the button labeled "Choose Image...". Select the desired bitmap image. Click "OK" to leave the Customize Macro Button dialog box. The new macro button will appear on the toolbar. A better description of this process can be found at: http://help.solidworks.com/2019/english/SolidWorks/sldworks/t_assigning_macro_toolbar_button.htm?verRedirect=1


EXPORT SETTINGS IN SOLIDWORKS
The quality of the export is controlled in the Solidworks Save command. Here are the settings used for testing. They can likely be adjusted but that hasn't been tested yet.

STL:
-Output: Binary
-Unit: Inches. These must match the import units on your 3D printing slicer program.
-Resolution: Use whatever you want. Finer is better
-Show STL before file saving [x]

STEP
-Output as: Solid/Surface geometry
-Export face/edge properties [x]
-Split periodic faces [x]


DETAILS
-Language: Visual Basic
-Solidworks version(s): 2019 sp3
-OS: Windows 10 x64 Enterprise build 17134.829


TROUBLESHOOTING
A good rule of thumb is that if you can export an STL using the normal process in Solidworks and successfully process it in a 3D printer slicer program, Body Export Macro should work. Here are some common issues and how to solve them.

-Issue: When saving a file as an STL, it successfully exports but when imported into a slicer there are errors. The geometry may be visible but the slicer may not be able to process it.
-Resolution: The units of the STL and what the slicer is expecting to be imported may be different. In Solidworks, navigate to Tools > Options > Export. Select STL from the drop down menu and set the file format as inches. In Insight (the slicing program for the Fortus 400mc), navigate to File > Preferences. Set the default STL units to inches. 

-Issue: Not all of your latest changes are being captured in the exported file.
-Resolution: Make sure the file is saved before running the export utility. This ensures the most recent updates are captured in the 3D part file.

-Issue: When exporting a file as a STEP, the macro counts the body but doesn't export it. 
-Resolution: The name of the body not being exported successfully may have a "<" or ">" symbol in it. Solidworks doesn't like the use of this symbol and will fail silently. To bypass the error, simply rename the body to remove any special characters. Note that this will not affect STL files.